{ ... }:
{
  programs.git = {
    enable = true;
    userName = "James King";
    userEmail = "jameshdking@gmail.com";
    extraConfig = {
      init.defaultBranch = "main";
      pull.rebase = true;
      push.autoSetupRemote = true;
      rerere.enabled = true;
      column.ui = "auto";
      maintenance.auto = false;
      maintenance.strategy = "incremental";
    };
    aliases = {
      co = "checkout";
      blame = "blame -w -C -C -C";
    };
  };
}

