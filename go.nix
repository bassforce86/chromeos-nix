{ pkgs, ... }:
{
  programs.go = {
    enable = true;
  };

  home.packages = [
    pkgs.go-tools
    pkgs.gopls
    pkgs.hugo
    pkgs.delve
  ];
}
