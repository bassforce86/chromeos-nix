{ inputs, system, ... }:

{
  imports = [
    ./git.nix
    ./go.nix
    ./zsh.nix
  ];
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home = {
    username = "jk";
    homeDirectory = "/home/jk";
    stateVersion = "24.05"; # Please read the comment before changing.

    # The home.packages option allows you to install Nix packages into your
    # environment.
    packages = [
      inputs.nixvim.packages.${system}.default
    ];

    sessionVariables = {
      EDITOR = "nvim";
    };

    shellAliases = {
      vim = "nvim";
      vi = "nvim";
      update = "nix flake update";
      switch = "home-manager switch --flake ~/.config/home-manager && source ~/.zshrc";
      clean = "nix-collect-garbage -d";
    };
  };

  fonts.fontconfig.enable = true;

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
