{ lib, pkgs, ... }: {
  programs.zsh = {
    enable = true;
    enableCompletion = true;
    autosuggestion.enable = true;
    syntaxHighlighting.enable = true;

    initExtraFirst = ''
      . "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"
         . /home/jk/.nix-profile/etc/profile.d/nix.sh
    '';
  };

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
    enableZshIntegration = true;
  };

  programs.starship = {
    enable = true;
    settings = {
      add_newline = false;
      aws.disabled = true;
      line_break.disabled = true;
      container.disabled = true;
      git_branch.symbol = "</> ";
      format = lib.concatStrings[ 
      	"$username"
	"$shlvl"
	"$directory"
	"$git_branch"
	"$git_commit"
	"$git_state"
	"$git_metrics"
	"$git_status"
	"$package"
	"$golang"
	"$python"
	"$zig"
	"$buf"
	"$nix_shell"
	"$direnv"
	"$env_var"
	"$sudo"
	"$cmd_duration"
	"$time"
	"$status"
	"$shell"
	"$character"
      ];
    };
  };

  home.packages = [
    pkgs.bat
    pkgs.tree
    pkgs.ripgrep
    pkgs.fd
    pkgs.glow
    pkgs.fzf
    pkgs.fastfetch
    pkgs.unzip
    pkgs.jq
    pkgs.yq-go
    pkgs.eza
    pkgs.dnsutils
    pkgs.which
    pkgs.gnumake
    pkgs.gccgo
  ];
}


